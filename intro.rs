fn main(){
    // PRINT
    let hellotxt = "hello, world!";
    println!("{}", hellotxt);

    // OPERATORS
    let mut number: i32 = 5;
    println!("{}",number);
    number += 1;
    println!("plus one = {}",number);
    number -= 2;
    println!("minus 2 = {}",number);
    number /= 2;
    println!("divided 2 = {}",number);
    number *= 2;
    println!("multiply 2 = {}",number);

    let boolean: bool = true;
    println!("{}",boolean);
    let boolean: bool = false;
    println!("{}",boolean);

    // LOOPS
    for i in 90..100{
        if i %2 == 0 {
            println!("{} even", i);
        } else {
            println!("{} odd", i);
        }
    }

    // VECTORS
    let mut v = vec![0,1,2,3];
    println!("entire vector: {:?}",v);
    v.push(4);
    println!("entire vector: {:?}",v);
    for element in v {
        println!("vector element: {}", element);
    }

    let mut v1 = vec![5,4,3,2,1,1,0];
    println!("v1 is {:?}",v1);
    v1.sort();
    println!("v1 sorted is {:?}",v1);
    v1.dedup();
    println!("v1 deduplicated is {:?}",v1);

    // FUNCTIONS
    fn add(x: i32, y:i32) -> i32 {
        let z = x + y;
        return z;
    }
    let z = add(1,2);
    println!("{}",z);

    // ASERTIONS
    let firstnum = 42;
    let secondnum = 43;
    assert_eq!(firstnum,secondnum);
}
